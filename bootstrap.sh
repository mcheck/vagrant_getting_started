#!/usr/bin/env bash

apt-get update
apt-get install -y apache2
rm -fr /var/www
ln -fs /vagrant /var/www